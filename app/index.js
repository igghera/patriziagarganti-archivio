import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, hashHistory, Route, IndexRoute } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import 'react-redux-toastr/src/styles/index.scss';
import ReduxToastr from 'react-redux-toastr';
import configureStore from './store/configureStore';

import App from './containers/App';
import ProductDetailPage from './containers/ProductDetailPage';
import SearchView from './containers/SearchView';
import CreateNewProductPage from './containers/CreateNewProductPage';

import './app.global.css';

const store = configureStore();
const history = syncHistoryWithStore(hashHistory, store);

render(
  <Provider store={store}>
    <div>
      <Router history={history}>
        <Route path="/" component={App}>
          <IndexRoute component={SearchView} />
          <Route path="/product/:itemId" component={ProductDetailPage} />
          <Route path="/new" component={CreateNewProductPage} />
        </Route>
      </Router>
      <ReduxToastr
        position="bottom-left"
        transitionIn="fadeIn"
        transitionOut="fadeOut"
      />
    </div>
  </Provider>,
  document.getElementById('root')
);
