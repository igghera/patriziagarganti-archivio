import config from '../config';

export function getArticoli(input, callback) {
  fetch(`${config.apiBaseUrl}/getJson.php?column=codice_articolo`)
    .then(response => response.json())
    .then(jsonResponse => callback(null, { options: jsonResponse }))
    .catch(error => console.log(error));
}

export function getAccessori() {
  return fetch(`${config.apiBaseUrl}/getJson.php?column=accessori`)
    .then(response => response.json());
}

export function getClienti() {
  return fetch(`${config.apiBaseUrl}/getJson.php?column=cliente`)
    .then(response => response.json());
}

export function getCollezioni() {
  return fetch(`${config.apiBaseUrl}/getJson.php?column=collezione`)
    .then(response => response.json());
}

export function getFiniture() {
  return fetch(`${config.apiBaseUrl}/getJson.php?column=finitura`)
    .then(response => response.json());
}

export function getForme() {
  return fetch(`${config.apiBaseUrl}/getJson.php?column=forma`)
    .then(response => response.json());
}

export function getMarchi() {
  return fetch(`${config.apiBaseUrl}/getJson.php?column=marchio`)
    .then(response => response.json());
}

export function getParalumi() {
  return fetch(`${config.apiBaseUrl}/getJson.php?column=paralume`)
    .then(response => response.json());
}

export function getParticolari() {
  return fetch(`${config.apiBaseUrl}/getJson.php?column=particolari`)
    .then(response => response.json());
}

export function getGeneri() {
  return fetch(`${config.apiBaseUrl}/getJson.php?column=type`)
    .then(response => response.json());
}

export function getTipologie() {
  return fetch(`${config.apiBaseUrl}/getJson.php?column=tipologia`)
    .then(response => response.json());
}
