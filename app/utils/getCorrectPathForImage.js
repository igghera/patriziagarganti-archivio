import config from '../config';

/**
 * Nel caso in cui il prodotto sia stato inserito da Temera in passato,
 * il path delle immagini inizia con `/`, ma non e' quello giusto perche' nella
 * cartella delle foto ci sono `/original` e `/thumbs` per cui qui operiamo una
 * trasformazione per puntare al file giusto
 */
export default function getCorrectPathForImage(imagePath) {
  if (imagePath.charAt(0) === '/') {
    const pathArray = imagePath.substring(1).split('/');
    const realPath = `${pathArray[0]}/original/${pathArray[1]}`;

    return `${config.apiBaseUrl}/product-images/${realPath}`;
  }

  // Se il path non inizia con "/", non importa la trasformazione perche' e'
  // gia' quello corretto
  return `${config.apiBaseUrl}/product-images/${imagePath}`;
}
