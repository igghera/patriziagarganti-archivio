import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import SelectInput from '../SelectInput';
import CreatableSelectInput from '../CreatableSelectInput';
import SpinnerFigo from '../SpinnerFigo';
import './style.css';
import { reloadFilters } from '../../actions/filters';

import { getArticoli } from '../../utils/fetchJson';

class CreateProductForm extends Component {
  componentDidMount() {
    this.props.dispatch(reloadFilters());
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit}>
        <div className="row CreateNewProductPage__FormRow CreateNewProductPage__FormRow--first">
          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="tipo">Genere</label>
            <Field
              name="type"
              component={CreatableSelectInput}
              placeholder="Seleziona"
              options={this.props.filters.generi}
              simpleValue
            />
          </div>

          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="codice_articolo">Codice articolo</label>
            <Field name="codice_articolo" component="input" type="text" className="CreateProductForm__Input" />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="larghezza">Larghezza</label>
            <Field name="larghezza" component="input" type="text" className="CreateProductForm__Input" />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="altezza">Altezza</label>
            <Field name="altezza" component="input" type="text" className="CreateProductForm__Input" />
          </div>
        </div>
        <div className="row CreateNewProductPage__FormRow">
          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="profondita">Profondità</label>
            <Field name="profondita" component="input" type="text" className="CreateProductForm__Input" />
          </div>

          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="lampadine_numero">Numero lampadine</label>
            <Field name="lampadine_numero" component="input" type="text" className="CreateProductForm__Input" />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="lampadine_watt">Watt lampadine</label>
            <Field name="lampadine_watt" component="input" type="text" className="CreateProductForm__Input" />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="lampadine_attacco">Attacco lamapdine</label>
            <Field name="lampadine_attacco" component="input" type="text" className="CreateProductForm__Input" />
          </div>
        </div>
        <div className="row CreateNewProductPage__FormRow">
          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="volume">Volume</label>
            <Field name="volume" component="input" type="text" className="CreateProductForm__Input" />
          </div>

          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="peso">Peso</label>
            <Field name="peso" component="input" type="text" className="CreateProductForm__Input" />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="descrizione_articolo">Descrizione articolo</label>
            <Field name="descrizione_articolo" component="input" type="text" className="CreateProductForm__Input" />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="tipologia">Tipologia</label>
            <Field
              name="tipologia"
              component={CreatableSelectInput}
              placeholder="Seleziona"
              options={this.props.filters.accessori}
              simpleValue
            />
          </div>
        </div>
        <div className="row CreateNewProductPage__FormRow">
          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="marchio">Marchio</label>
            <Field
              name="marchio"
              component={CreatableSelectInput}
              placeholder="Seleziona"
              options={this.props.filters.marchi}
              simpleValue
            />
          </div>

          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="collezione">Collezione</label>
            <Field
              name="collezione"
              component={CreatableSelectInput}
              placeholder="Seleziona"
              options={this.props.filters.collezioni}
              simpleValue
            />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="cliente">Cliente</label>
            <Field
              name="cliente"
              component={CreatableSelectInput}
              placeholder="Seleziona"
              options={this.props.filters.clienti}
              simpleValue
            />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="anno">Anno</label>
            <Field name="anno" component="input" type="number" className="CreateProductForm__Input" />
          </div>
        </div>
        <div className="row CreateNewProductPage__FormRow">
          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="forma">Forma</label>
            <Field
              name="forma"
              component={CreatableSelectInput}
              placeholder="Seleziona"
              options={this.props.filters.forme}
              simpleValue
            />
          </div>

          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="finitura">Finitura</label>
            <Field
              name="finitura"
              component={CreatableSelectInput}
              placeholder="Seleziona"
              options={this.props.filters.finiture}
              simpleValue
            />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="paralume">Paralume</label>
            <Field
              name="paralume"
              component={CreatableSelectInput}
              placeholder="Seleziona"
              options={this.props.filters.paralumi}
              simpleValue
            />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="accessori">Accessori</label>
            <Field
              name="accessori"
              component={CreatableSelectInput}
              placeholder="Seleziona"
              options={this.props.filters.accessori}
              simpleValue
            />
          </div>
        </div>
        <div className="row CreateNewProductPage__FormRow">
          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="officina">Officina</label>
            <Field
              name="officina"
              component={SelectInput}
              options={[{ value: 'no', label: 'No' }, { value: 'sì', label: 'Sì' }]}
              simpleValue
            />
          </div>

          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="path_area_di_lavoro">Percorso sul server</label>
            <Field name="path_area_di_lavoro" component="input" type="text" className="CreateProductForm__Input" />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="diametro">Diametro</label>
            <Field name="diametro" component="input" type="text" className="CreateProductForm__Input" />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="note_design">Note design</label>
            <Field name="note_design" component="input" type="text" className="CreateProductForm__Input" />
          </div>
        </div>
        <div className="row CreateNewProductPage__FormRow">
          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="progetto">Progetto</label>
            <Field name="progetto" component="input" type="text" className="CreateProductForm__Input" placeholder="Cos'e questo?" />
          </div>

          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="particolari">Particolari</label>
            <Field
              name="particolari"
              component={CreatableSelectInput}
              options={this.props.filters.particolari}
              simpleValue
            />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="articolo_di_riferimento">Articolo di riferimento</label>
            <Field
              name="articoli_correlati"
              component={SelectInput}
              loadOptions={getArticoli}
              simpleValue
              multi
              async
            />
          </div>

          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="articoli_correlati">Articoli correlati</label>
            <Field
              name="articoli_correlati"
              component={SelectInput}
              loadOptions={getArticoli}
              simpleValue
              multi
              async
            />
          </div>
        </div>
        <div className="row CreateNewProductPage__FormRow">
          <div className="col-md-3 col-md-4 col-sm-6 col-xs-12">
            <label className="CreateProductForm__Label" htmlFor="varianti">Varianti</label>
            <Field name="varianti" component="input" type="text" className="CreateProductForm__Input" placeholder="Qui che cosa ci mettiamo?" />
          </div>
        </div>
        <div className="CreateNewProductPage__SubmitContainer">
          <button
            type="submit"
            className="CreateNewProductPage__SubmitBtn"
            disabled={this.props.submitting}
          >
            { !this.props.submitting && 'Salva' }
            { this.props.submitting && <SpinnerFigo /> }
          </button>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  filters: state.filters
});

const withReduxForm = reduxForm({
  form: 'createNewProduct'
})(CreateProductForm);

export default connect(mapStateToProps)(withReduxForm);
