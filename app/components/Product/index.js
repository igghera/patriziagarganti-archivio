import React, { Component } from 'react';
import { Link } from 'react-router';
import './style.css';
import getCorrectPathForImage from '../../utils/getCorrectPathForImage';

export default class Product extends Component {
  getPhoto() {
    if (this.props.viewModel.photos.length === 0) {
      return 'http://placehold.it/300/cccccc/000000?text=FOTO+NON+PERVENUTA';
    }

    return getCorrectPathForImage(this.props.viewModel.photos[0].path);
  }

  render() {
    const { viewModel: product } = this.props;

    return (
      <Link to={`/product/${product.id}`} className="Product">
        <div
          className="Product__Image"
          style={{
            backgroundImage: `url(${this.getPhoto()})`
          }}
        />

        <h6 className="Product__Name">{ product.codice_articolo }</h6>
      </Link>
    );
  }
}
