import React, { Component } from 'react';
import { connect } from 'react-redux';
import InputRange from 'react-input-range';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import 'react-input-range/dist/react-input-range.css';
import './style.css';
import { reloadFilters } from '../../actions/filters';
import { search, resetSearchForm } from '../../actions/search';

class FilterForm extends Component {
  handleLarghezzaChange = (component, larghezza) => this.props.dispatch(search({ larghezza }));
  handleAltezzaChange = (component, altezza) => this.props.dispatch(search({ altezza }));
  handleProfonditaChange = (component, profondita) => this.props.dispatch(search({ profondita }));
  handleDiametroChange = (component, diametro) => this.props.dispatch(search({ diametro }));
  handleLuciChange = (component, luci) => this.props.dispatch(search({ luci }));
  handleAnnoChange = (component, anno) => this.props.dispatch(search({ anno }));
  handleKeywordsChange = e => {
    this.props.dispatch(search({ keywords: e.target.value }));
  }
  handleTipologiaChange = tipologia => this.props.dispatch(search({ tipologia }));
  handleFormaChange = forma => this.props.dispatch(search({ forma }));
  handleMarchioChange = marchio => this.props.dispatch(search({ marchio }));
  handleCollezioneChange = collezione => this.props.dispatch(search({ collezione }));
  handleAccessoriChange = accessori => this.props.dispatch(search({ accessori }));
  handleClienteChange = cliente => this.props.dispatch(search({ cliente }));
  handleFinitureChange = finitura => this.props.dispatch(search({ finitura }));
  handleGenereChange = genere => this.props.dispatch(search({ genere }));
  handleParalumeChange = paralume => this.props.dispatch(search({ paralume }));
  handleParticolariChange = particolari => this.props.dispatch(search({ particolari }));

  handleResetClick = () => {
    this.props.dispatch(resetSearchForm());
  }

  handleClearKeywords = e => {
    e.preventDefault();
    this.props.dispatch(search({ keywords: '' }));
  }

  componentDidMount() {
    this.props.dispatch(reloadFilters());
  }

  render() {
    return (
      <div className="FilterForm">
        <div className="FilterForm__BtnClearFilters sidebar-widget">
          <button
            className="btn btn-md btn-black-line btn-block"
            onClick={this.handleResetClick}
          >
            Reset Filtri
          </button>
        </div>

        <div className="sidebar-widget">
          <h5>Codice Articolo</h5>
          <div className="widget-search">
            <input
              onChange={this.handleKeywordsChange}
              className="FilterForm__KeywordsInput form-full input-lg"
              type="text"
              value={this.props.form.keywords}
            />
            <a
              href="#"
              className="FilterForm__BtnClearKeywords"
              onClick={this.handleClearKeywords}
            >&times;</a>
          </div>
        </div>

        <div
          className="FilterForm__InputRange sidebar-widget"
          style={{ paddingTop: '0' }}
        >
          <h5>Larghezza</h5>
          <InputRange
            maxValue={600}
            minValue={0}
            step={10}
            value={this.props.form.larghezza}
            onChange={this.handleLarghezzaChange}
          />
        </div>

        <div className="FilterForm__InputRange sidebar-widget">
          <h5>Altezza</h5>
          <InputRange
            maxValue={4580}
            minValue={0}
            step={10}
            value={this.props.form.altezza}
            onChange={this.handleAltezzaChange}
          />
        </div>

        <div className="FilterForm__InputRange sidebar-widget">
          <h5>Profondita</h5>
          <InputRange
            maxValue={277}
            minValue={0}
            step={10}
            value={this.props.form.profondita}
            onChange={this.handleProfonditaChange}
          />
        </div>

        <div className="FilterForm__InputRange sidebar-widget">
          <h5>Diametro</h5>
          <InputRange
            maxValue={1150}
            minValue={0}
            step={10}
            value={this.props.form.diametro}
            onChange={this.handleDiametroChange}
          />
        </div>

        <div className="FilterForm__InputRange sidebar-widget">
          <h5>Luci</h5>
          <InputRange
            maxValue={156}
            minValue={0}
            step={1}
            value={this.props.form.luci}
            onChange={this.handleLuciChange}
          />
        </div>

        <div className="FilterForm__InputRange sidebar-widget">
          <h5>Anno</h5>
          <InputRange
            maxValue={new Date().getFullYear()}
            minValue={2011}
            step={1}
            value={this.props.form.anno}
            onChange={this.handleAnnoChange}
          />
        </div>

        <div className="sidebar-widget" style={{ marginTop: '20px' }}>
          <h5>Tipologia</h5>
          <Select
            noResultsText=""
            placeholder="Nessuno"
            onChange={this.handleTipologiaChange}
            value={this.props.form.tipologia}
            options={this.props.filters.tipologie}
            multi
            simpleValue
          />
        </div>

        <div className="sidebar-widget" style={{ marginTop: '20px' }}>
          <h5>Forma</h5>
          <Select
            noResultsText=""
            placeholder="Nessuno"
            onChange={this.handleFormaChange}
            value={this.props.form.forma}
            options={this.props.filters.forme}
            multi
            simpleValue
          />
        </div>

        <div className="sidebar-widget" style={{ marginTop: '20px' }}>
          <h5>Collezione</h5>
          <Select
            noResultsText=""
            placeholder="Nessuno"
            onChange={this.handleCollezioneChange}
            value={this.props.form.collezione}
            options={this.props.filters.collezioni}
            multi
            simpleValue
          />
        </div>

        <div className="sidebar-widget" style={{ marginTop: '20px' }}>
          <h5>Marchio</h5>
          <Select
            noResultsText=""
            placeholder="Nessuno"
            onChange={this.handleMarchioChange}
            value={this.props.form.marchio}
            options={this.props.filters.marchi}
            multi
            simpleValue
          />
        </div>

        <div className="sidebar-widget" style={{ marginTop: '20px' }}>
          <h5>Accessori</h5>
          <Select
            noResultsText=""
            placeholder="Nessuno"
            onChange={this.handleAccessoriChange}
            value={this.props.form.accessori}
            options={this.props.filters.accessori}
            multi
            simpleValue
          />
        </div>

        <div className="sidebar-widget" style={{ marginTop: '20px' }}>
          <h5>Cliente</h5>
          <Select
            noResultsText=""
            placeholder="Nessuno"
            onChange={this.handleClienteChange}
            value={this.props.form.cliente}
            options={this.props.filters.clienti}
            multi
            simpleValue
          />
        </div>

        <div className="sidebar-widget" style={{ marginTop: '20px' }}>
          <h5>Finitura</h5>
          <Select
            noResultsText=""
            placeholder="Nessuno"
            onChange={this.handleFinitureChange}
            value={this.props.form.finitura}
            options={this.props.filters.finiture}
            multi
            simpleValue
          />
        </div>

        <div className="sidebar-widget" style={{ marginTop: '20px' }}>
          <h5>Genere</h5>
          <Select
            noResultsText=""
            placeholder="Nessuno"
            onChange={this.handleGenereChange}
            value={this.props.form.genere}
            options={this.props.filters.generi}
            multi
            simpleValue
          />
        </div>

        <div className="sidebar-widget" style={{ marginTop: '20px' }}>
          <h5>Paralume</h5>
          <Select
            noResultsText=""
            placeholder="Nessuno"
            onChange={this.handleParalumeChange}
            value={this.props.form.paralume}
            options={this.props.filters.paralumi}
            multi
            simpleValue
          />
        </div>

        <div className="sidebar-widget" style={{ marginTop: '20px' }}>
          <h5>Particolari</h5>
          <Select
            noResultsText=""
            placeholder="Nessuno"
            onChange={this.handleParticolariChange}
            value={this.props.form.particolari}
            options={this.props.filters.particolari}
            multi
            simpleValue
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  form: state.search,
  filters: state.filters
});

export default connect(mapStateToProps)(FilterForm);
