import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import SelectInput from '../SelectInput';
import { getArticoli } from '../../utils/fetchJson';
import { reloadFilters } from '../../actions/filters';
import './style.css';

class UpdateProductForm extends Component {
  componentDidMount() {
    this.props.dispatch(reloadFilters());
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit}>

        <section id="general-info">
          <h2 className="ProductTable__Heading">Info generali</h2>

          <table className="ProductTable table table-border table-hover">
            <tbody>
              <tr>
                <td className="ProductTable__FirstCol">Genere</td>
                <td>
                  <Field
                    name="type"
                    component={SelectInput}
                    placeholder="Seleziona"
                    options={this.props.filters.generi}
                    simpleValue
                  />
                </td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Marchio</td>
                <td>
                  <Field
                    name="marchio"
                    component={SelectInput}
                    placeholder="Seleziona"
                    options={this.props.filters.marchi}
                    simpleValue
                  />
                </td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Collezione</td>
                <td>
                  <Field
                    name="collezione"
                    component={SelectInput}
                    placeholder="Seleziona"
                    options={this.props.filters.collezioni}
                    simpleValue
                  />
                </td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Tipologia</td>
                <td>
                  <Field
                    name="tipologia"
                    component={SelectInput}
                    placeholder="Seleziona"
                    options={this.props.filters.tipologie}
                    simpleValue
                  />
                </td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Codice articolo</td>
                <td>
                  <Field name="codice_articolo" component="input" type="text" className="CreateProductForm__Input" />
                </td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Descrizione</td>
                <td>
                  <Field name="descrizione_articolo" component="textarea" type="text" className="CreateProductForm__Input" />
                </td>
              </tr>
            </tbody>
          </table>
        </section>

        <section id="details">
          <h2 className="ProductTable__Heading">Dettagli</h2>

          <table className="ProductTable table table-border table-hover">
            <tbody>
              <tr>
                <td className="ProductTable__FirstCol">Finitura</td>
                <td>
                  <Field
                    name="finitura"
                    component={SelectInput}
                    placeholder="Seleziona"
                    options={this.props.filters.finiture}
                    simpleValue
                  />
                </td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Paralume</td>
                <td>
                  <Field
                    name="paralume"
                    component={SelectInput}
                    placeholder="Seleziona"
                    options={this.props.filters.paralumi}
                    simpleValue
                  />
                </td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Accessori</td>
                <td>
                  <Field
                    name="accessori"
                    component={SelectInput}
                    placeholder="Seleziona"
                    options={this.props.filters.accessori}
                    simpleValue
                  />
                </td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Particolari</td>
                <td>
                  <Field
                    name="particolari"
                    component={SelectInput}
                    options={this.props.filters.particolari}
                    simpleValue
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </section>

        <section id="dimensions">
          <h2 className="ProductTable__Heading">Misure</h2>

          <table className="ProductTable table table-border table-hover">
            <tbody>
              <tr>
                <td className="ProductTable__FirstCol">Larghezza (cm)</td>
                <td><Field name="larghezza" component="input" type="text" className="CreateProductForm__Input" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Altezza (cm)</td>
                <td><Field name="altezza" component="input" type="text" className="CreateProductForm__Input" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Profondità (cm)</td>
                <td><Field name="profondita" component="input" type="text" className="CreateProductForm__Input" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Diametro (cm)</td>
                <td><Field name="diametro" component="input" type="text" className="CreateProductForm__Input" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Volume (m³)</td>
                <td><Field name="volume" component="input" type="text" className="CreateProductForm__Input" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Peso (kg)</td>
                <td><Field name="peso" component="input" type="text" className="CreateProductForm__Input" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Forma</td>
                <td>
                  <Field
                    name="forma"
                    component={SelectInput}
                    placeholder="Seleziona"
                    options={this.props.filters.forme}
                    simpleValue
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </section>

        <section id="lampadine">
          <h2 className="ProductTable__Heading">Luci</h2>

          <table className="ProductTable table table-border table-hover">
            <tbody>
              <tr>
                <td className="ProductTable__FirstCol">Numero lampadine</td>
                <td><Field name="lampadine_numero" component="input" type="text" className="CreateProductForm__Input" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Watt lampadine</td>
                <td><Field name="lampadine_watt" component="input" type="text" className="CreateProductForm__Input" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Attacco lampadine</td>
                <td><Field name="lampadine_attacco" component="input" type="text" className="CreateProductForm__Input" /></td>
              </tr>
            </tbody>
          </table>
        </section>

        <section id="details">
          <h2 className="ProductTable__Heading">Altro</h2>

          <table className="ProductTable table table-border">
            <tbody>
              <tr>
                <td className="ProductTable__FirstCol">Cliente</td>
                <td><Field name="cliente" component="input" type="text" className="CreateProductForm__Input" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Anno</td>
                <td><Field name="anno" component="input" type="number" className="CreateProductForm__Input" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Officina</td>
                <td>
                  <Field
                    name="officina"
                    component={SelectInput}
                    options={[{ value: 'no', label: 'No' }, { value: 'sì', label: 'Sì' }]}
                    simpleValue
                  />
                </td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Note</td>
                <td><Field name="note_design" component="textarea" type="text" className="CreateProductForm__Input" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Progetto</td>
                <td><Field name="progetto" component="input" type="text" className="CreateProductForm__Input" placeholder="Cos'e questo?" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Articolo di riferimento</td>
                <td>
                  <Field
                    name="articolo_di_riferimento"
                    component={SelectInput}
                    loadOptions={getArticoli}
                    simpleValue
                    async
                  />
                </td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Articoli correlati</td>
                <td>
                  <Field
                    name="articoli_correlati"
                    component={SelectInput}
                    loadOptions={getArticoli}
                    simpleValue
                    async
                    multi
                  />
                </td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Varianti</td>
                <td><Field name="varianti" component="input" type="text" className="CreateProductForm__Input" placeholder="Qui che cosa ci mettiamo?" /></td>
              </tr>
              <tr>
                <td className="ProductTable__FirstCol">Percorso sul file server</td>
                <td><Field name="path_area_di_lavoro" component="input" type="text" className="CreateProductForm__Input" /></td>
              </tr>
            </tbody>
          </table>
        </section>

        <button className="btn btn-lg btn-black btn-block" type="submit">
          Salva modifiche
        </button>
      </form>
    );
  }
}

UpdateProductForm = reduxForm({
  form: 'updateProductForm'
})(UpdateProductForm);

UpdateProductForm = connect(state => {
  const model = {
    ...state.product,
    articoli_correlati: state.product.articoli_correlati ?
      state.product.articoli_correlati.map(item => item.id).join(',') :
      ''
  };

  return {
    initialValues: model,
    filters: state.filters
  };
})(UpdateProductForm);

export default UpdateProductForm;
