import React from 'react';
import './style.css';

export default function SpinnerBalls() {
  return <div className="cp-spinner cp-balls" />;
}
