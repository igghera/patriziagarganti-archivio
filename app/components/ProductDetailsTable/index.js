import React, { Component } from 'react';
import { Link } from 'react-router';
import getCorrectPathForImage from '../../utils/getCorrectPathForImage';

export default class ProductDetailsTable extends Component {
  render() {
    const { product } = this.props;

    return (
      <div>
        <section id="general-info">
          <h2 className="ProductTable__Heading">Info generali</h2>

          <table className="ProductTable table table-border table-hover">
            <tbody>
              { product.type &&
                <tr>
                  <td className="ProductTable__FirstCol">Genere</td>
                  <td>{ product.type }</td>
                </tr>
              }
              { product.marchio &&
                <tr>
                  <td className="ProductTable__FirstCol">Marchio</td>
                  <td>{ product.marchio }</td>
                </tr>
              }
              { product.collezione &&
                <tr>
                  <td className="ProductTable__FirstCol">Collezione</td>
                  <td>{ product.collezione }</td>
                </tr>
              }
              { product.tipologia &&
                <tr>
                  <td className="ProductTable__FirstCol">Tipologia</td>
                  <td>{ product.tipologia }</td>
                </tr>
              }
              { product.codice_articolo &&
                <tr>
                  <td className="ProductTable__FirstCol">Codice articolo</td>
                  <td>{ product.codice_articolo }</td>
                </tr>
              }
              { product.descrizione_articolo &&
                <tr>
                  <td className="ProductTable__FirstCol">Descrizione</td>
                  <td>{ product.descrizione_articolo }</td>
                </tr>
              }
            </tbody>
          </table>
        </section>

        {
          (product.finitura !== null ||
          product.paralume !== null ||
          product.particolari !== null ||
          product.accessori !== null) &&
          <section id="details">
            <h2 className="ProductTable__Heading">Dettagli</h2>

            <table className="ProductTable table table-border table-hover">
              <tbody>

                { product.finitura &&
                  <tr>
                    <td className="ProductTable__FirstCol">Finitura</td>
                    <td>{ product.finitura }</td>
                  </tr>
                }
                { product.paralume &&
                  <tr>
                    <td className="ProductTable__FirstCol">Paralume</td>
                    <td>{ product.paralume }</td>
                  </tr>
                }
                { product.accessori &&
                  <tr>
                    <td className="ProductTable__FirstCol">Accessori</td>
                    <td>{ product.accessori }</td>
                  </tr>
                }
                { product.particolari &&
                  <tr>
                    <td className="ProductTable__FirstCol">Particolari</td>
                    <td>{ product.particolari }</td>
                  </tr>
                }
              </tbody>
            </table>
          </section>
        }

        <section id="dimensions">
          <h2 className="ProductTable__Heading">Misure</h2>

          <table className="ProductTable table table-border table-hover">
            <tbody>
              { product.larghezza &&
                <tr>
                  <td className="ProductTable__FirstCol">Larghezza</td>
                  <td>{ product.larghezza } cm</td>
                </tr>
              }
              { product.altezza &&
                <tr>
                  <td className="ProductTable__FirstCol">Altezza</td>
                  <td>{ product.altezza } cm</td>
                </tr>
              }
              { product.profondita &&
                <tr>
                  <td className="ProductTable__FirstCol">Profondità</td>
                  <td>{ product.profondita } cm</td>
                </tr>
              }
              { product.diametro &&
                <tr>
                  <td className="ProductTable__FirstCol">Diametro</td>
                  <td>{ product.diametro } cm</td>
                </tr>
              }
              { product.volume &&
                <tr>
                  <td className="ProductTable__FirstCol">Volume</td>
                  <td>{ product.volume } m³</td>
                </tr>
              }
              { product.peso &&
                <tr>
                  <td className="ProductTable__FirstCol">Peso</td>
                  <td>{ product.peso } Kg</td>
                </tr>
              }
              { product.forma &&
                <tr>
                  <td className="ProductTable__FirstCol">Forma</td>
                  <td>{ product.forma }</td>
                </tr>
              }
            </tbody>
          </table>
        </section>

        {
          (product.lampadine_numero !== null ||
          product.lampadine_watt !== null ||
          product.lampadine_attacco !== null) &&
          <section id="lampadine">
            <h2 className="ProductTable__Heading">Luci</h2>

            <table className="ProductTable table table-border table-hover">
              <tbody>
                { product.lampadine_numero &&
                  <tr>
                    <td className="ProductTable__FirstCol">Numero lampadine</td>
                    <td>{ product.lampadine_numero }</td>
                  </tr>
                }
                { product.lampadine_watt &&
                  <tr>
                    <td className="ProductTable__FirstCol">Watt lampadine</td>
                    <td>{ product.lampadine_watt }</td>
                  </tr>
                }
                { product.lampadine_attacco &&
                  <tr>
                    <td className="ProductTable__FirstCol">Attacco lampadine</td>
                    <td>{ product.lampadine_attacco }</td>
                  </tr>
                }
              </tbody>
            </table>
          </section>
        }

        {
          (product.cliente !== null ||
          product.anno !== null ||
          product.officina !== null ||
          product.note_design !== null ||
          product.progetto !== null ||
          product.articolo_di_riferimento !== null ||
          product.articoli_correlati !== null ||
          product.varianti !== null) &&
          <section id="details">
            <h2 className="ProductTable__Heading">Altro</h2>

            <table className="ProductTable table table-border">
              <tbody>

                { product.cliente &&
                  <tr>
                    <td className="ProductTable__FirstCol">Cliente</td>
                    <td>{ product.cliente }</td>
                  </tr>
                }
                { product.anno &&
                  <tr>
                    <td className="ProductTable__FirstCol">Anno</td>
                    <td>{ product.anno }</td>
                  </tr>
                }
                { product.officina &&
                  <tr>
                    <td className="ProductTable__FirstCol">Officina</td>
                    <td>{ product.officina }</td>
                  </tr>
                }
                { product.note_design &&
                  <tr>
                    <td className="ProductTable__FirstCol">Note</td>
                    <td>{ product.note_design }</td>
                  </tr>
                }
                { product.progetto &&
                  <tr>
                    <td className="ProductTable__FirstCol">Progetto</td>
                    <td>{ product.progetto }</td>
                  </tr>
                }
                { product.articolo_di_riferimento &&
                  <tr>
                    <td className="ProductTable__FirstCol">Articolo di riferimento</td>
                    <td>{ product.articolo_di_riferimento }</td>
                  </tr>
                }
                { product.articoli_correlati &&
                  <tr>
                    <td className="ProductTable__FirstCol">Articoli correlati</td>
                    <td>
                      {
                        product.articoli_correlati.map((correlato, i) => {
                          const pathToCorrelato = `/product/${correlato.id}`;
                          return (
                            <Link
                              key={i}
                              to={pathToCorrelato}
                              className="ProductTable__RelatedPhoto"
                            >
                              <img src={getCorrectPathForImage(correlato.photo.path)} />
                            </Link>
                          );
                        })
                      }
                    </td>
                  </tr>
                }
                { product.varianti &&
                  <tr>
                    <td className="ProductTable__FirstCol">Varianti</td>
                    <td>{ product.varianti }</td>
                  </tr>
                }
              </tbody>
            </table>
          </section>
        }
      </div>
    );
  }
}
