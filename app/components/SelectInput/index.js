import React from 'react';
import Select from 'react-select';

// A little hack to make `react-select` work with `redux-form`.
// See https://github.com/erikras/redux-form/issues/82

export default (props) => {
  if (props.async) {
    return (<Select.Async
      {...props}
      value={props.input.value}
      onChange={(value) => props.input.onChange(value)}
      onBlur={() => props.input.onBlur(props.input.value)}
      loadOptions={props.loadOptions}
    />);
  }

  return (<Select
    {...props}
    value={props.input.value}
    onChange={(value) => props.input.onChange(value)}
    onBlur={() => props.input.onBlur(props.input.value)}
    options={props.options}
  />);
};
