const config = {
  apiBaseUrl: (
    process.env.NODE_ENV === 'production' ?
      'http://192.0.0.197/archivio-api' :
      'http://archivio-api.local'
  )
};

export default config;
