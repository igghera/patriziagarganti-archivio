import React, { Component } from 'react';
import { connect } from 'react-redux';
import FilterForm from '../../components/FilterForm';
import Product from '../../components/Product';
import SpinnerFigo from '../../components/SpinnerFigo';
import './style.css';

class SearchView extends Component {
  renderLoadingOverlay() {
    return (
      <div className="SearchView__LoadingOverlay">
        <SpinnerFigo />
        <span className="SearchView__LoadingOverlay__Label">Carico...</span>
      </div>
    );
  }

  renderSearchResults() {
    return this.props.searchResults.map((product, i) =>
      <Product key={i} viewModel={product} />
    );
  }

  renderWelcomeMessage() {
    return (
      <div className="SearchView__NoResults">
        Inizia una ricerca usando i filtri sulla sinistra
      </div>
    );
  }

  renderNoResults() {
    return (
      <div className="SearchView__NoResults">
        Nessun risultato. <br />Prova a cambiare i filtri
      </div>
    );
  }

  render() {
    return (
      <div className="SearchView">
        <FilterForm />
        <div className="ProductList">
          { this.props.isSearching && this.renderLoadingOverlay() }
          { this.props.searchResults && this.renderSearchResults() }
          {
            this.props.searchResults &&
            this.props.searchResults.length === 0 &&
            this.renderNoResults()
          }
          {
            !this.props.searchResults &&
            this.renderWelcomeMessage()
          }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    searchResults: state.search.searchResults,
    isSearching: state.search.isSearching
  };
}

export default connect(mapStateToProps)(SearchView);
