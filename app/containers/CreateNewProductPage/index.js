import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import ReactTooltip from 'react-tooltip';
import { hashHistory } from 'react-router';
import { toastr } from 'react-redux-toastr';
import 'whatwg-fetch';
import CreateProductForm from '../../components/CreateProductForm';
import './style.css';
import config from '../../config';

export default class CreateNewProductPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      productImages: []
    };

    this.onDrop = this.onDrop.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleRemoveImage = this.handleRemoveImage.bind(this);
    this.toggleImageVisibilityOnWebsite = this.toggleImageVisibilityOnWebsite.bind(this);
  }

  onDrop(newImages) {
    this.setState({ productImages: [
      ...this.state.productImages,
      ...newImages.map(image => ({ visible: false, image }))]
    });
  }

  handleChange(fieldName, fieldValue) {
    this.setState({ [fieldName]: fieldValue });
  }

  handleSubmit(formValues) {
    const data = new FormData();
    data.append('formValues', JSON.stringify(formValues));

    this.state.productImages.forEach(productImage => {
      data.append(productImage.image.name, productImage.image);
    });

    return fetch(`${config.apiBaseUrl}/createNewProduct.php`, {
      method: 'POST',
      body: data
    })
    .then(response => response.json())
    .then(jsonResponse => {
      if (jsonResponse.status === 'OK') {
        toastr.success('Prodotto aggiunto', 'Bomba a mano');

        const newlyCreatedItemId = jsonResponse.newlyCreatedItemId;
        hashHistory.push(`/product/${newlyCreatedItemId}`);
        return true;
      }

      return console.log(jsonResponse);
    })
    .catch(e => {
      console.log(e);
    });
  }

  handleRemoveImage(indexToRemove) {
    this.setState({
      productImages: this.state.productImages.filter((image, index) => index !== indexToRemove)
    }, () => ReactTooltip.hide());
  }

  toggleImageVisibilityOnWebsite(imageIndex) {
    const productImages = this.state.productImages;
    productImages[imageIndex].visible = !productImages[imageIndex].visible;

    this.setState({ productImages });
    ReactTooltip.hide();
    ReactTooltip.rebuild();
  }

  render() {
    return (
      <div className="CreateNewProductPage container-fluid">
        <div className="row">
          <div className="col-md-3">
            <Dropzone onDrop={this.onDrop} accept="image/*">
              <div className="CreateNewProductPage__DropZoneInstructions">
                Trascina qui le immagini da caricare, o clicca per aprire una
                cartella e scegliere i file
              </div>
            </Dropzone>
          </div>

          <div className="col-md-9">
            {
              this.state.productImages.length > 0 &&
              <div className="CreateNewProductPage__ImagePreviewContainer">
                {
                  this.state.productImages.map((productImage, i) => (
                    <div className="CreateNewProductPage__ImagePreview" key={i}>
                      <img src={productImage.image.preview} />
                      <a
                        className="CreateNewProductPage__RemoveImageBtn"
                        onClick={() => this.handleRemoveImage(i)}
                        data-tip="Rimuovi questa immagine"
                      >
                        &times;
                      </a>
                      <a
                        data-tip="Vuoi che questa immagine sia visibile anche sul sito?"
                        className={
                          'CreateNewProductPage__BtnFlagAsVisibileOnWebsite ' + (
                            productImage.visible ?
                            'CreateNewProductPage__BtnFlagAsVisibileOnWebsite--visible' :
                            '')
                        }
                        onClick={() => this.toggleImageVisibilityOnWebsite(i)}
                      >
                        <i className={'fa ' + (productImage.visible ? 'fa-eye' : 'fa-eye-slash')} />
                      </a>
                      <ReactTooltip multiline />
                    </div>
                  ))
                }
              </div>
            }
          </div>
        </div>

        <CreateProductForm onSubmit={this.handleSubmit} />
      </div>
    );
  }
}
