import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { shell } from 'electron';
import ProductDetailsTable from '../../components/ProductDetailsTable';
import UpdateProductForm from '../../components/UpdateProductForm';
import SpinnerBalls from '../../components/SpinnerBalls';
import { fetchProduct } from '../../actions/api';
import config from '../../config';
import './style.css';
import getCorrectPathForImage from '../../utils/getCorrectPathForImage';

class ProductDetailPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedPhoto: 0,
      isEditing: false
    };

    this.renderThumbs = this.renderThumbs.bind(this);
    this.loadProduct = this.loadProduct.bind(this);
    this.selectPhoto = this.selectPhoto.bind(this);
    this.renderBigImage = this.renderBigImage.bind(this);
    this.openEditMode = this.openEditMode.bind(this);
    this.saveEdits = this.saveEdits.bind(this);
  }

  static propTypes = {
    params: PropTypes.object
  }

  componentDidMount() {
    this.loadProduct(this.props.params.itemId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params.itemId === this.props.params.itemId) return;
    this.loadProduct(nextProps.params.itemId);
  }

  loadProduct = itemId => {
    this.props.dispatch(fetchProduct(itemId));
  }

  apriCartellaSulFileServer = path => {
    shell.openItem(path.replace(/\\/g, '\\'));
  }

  selectPhoto(photoIndex) {
    this.setState({ selectedPhoto: photoIndex });
  }

  openEditMode() {
    this.setState({ isEditing: true });
  }

  saveEdits() {
    this.setState({ isEditing: false });
  }

  handleUpdateFormSubmit(formValues) {
    console.log(formValues);
    const data = new FormData();
    data.append('formValues', JSON.stringify(formValues));

    fetch(`${config.apiBaseUrl}/updateProduct.php`, {
      method: 'POST',
      body: data
    });
  }

  renderThumbs() {
    return (
      <div className="ProductDetailPage__ThumbsContainer container-fluid">
        <div className="row">
          {
            this.props.product.photos.map((photo, i) => (
              <div className="col-sm-4">
                <button
                  key={i}
                  className={
                    'ProductDetailPage__Thumb ' +
                    (
                      this.state.selectedPhoto === i ?
                      'ProductDetailPage__Thumb--selected' :
                      ''
                    )
                  }
                  onClick={() => this.selectPhoto(i)}
                >
                  <img
                    src={getCorrectPathForImage(photo.path)}
                    className="img-responsive"
                  />
                </button>
              </div>
            ))
          }
        </div>
      </div>
    );
  }

  renderBigImage() {
    const imagePath = this.props.product.photos[this.state.selectedPhoto].path;

    return (
      <img
        src={`${config.apiBaseUrl}/product-images/${imagePath}`}
        className="img-responsive ProductDetailPage__BigImage"
      />
    );
  }

  renderUpdateForm() {
    return <UpdateProductForm product={this.props.product} onSubmit={this.handleUpdateFormSubmit} />;
  }

  renderProductDetails() {
    return <ProductDetailsTable product={this.props.product} />;
  }

  render() {
    if (this.props.isLoading) {
      return (
        <div className="ProductDetailPage__Loading">
          <div>
            <SpinnerBalls className="ProductDetailPage__Spinner" />
            <p>Caricamento...</p>
          </div>
        </div>
      );
    }

    const { product } = this.props;

    return (
      <div className="ProductDetailPage container-fluid">
        <div className="row">
          <div className="col-md-5">

            { product.photos && product.photos.length > 0 && this.renderBigImage() }
            { product.photos && product.photos.length > 1 && this.renderThumbs() }

            <button
              className="ProductDetailPage__btn-back btn btn-lg btn-black btn-block"
              onClick={history.back}
            >
              Torna indietro
            </button>

            {
              product.path_area_di_lavoro &&
              <button
                className="btn btn-lg btn-black btn-block"
                onClick={() => this.apriCartellaSulFileServer(product.path_area_di_lavoro)}
              >
                Apri sul file server
              </button>
            }

            {
              product.scheda_tecnica_it &&
              <button
                className="btn btn-lg btn-black btn-block"
                onClick={() => shell.openItem(product.scheda_tecnica_it)}
              >
                Apri scheda tecnica
              </button>
            }

            {
              !this.state.isEditing &&
              <button
                className="btn btn-lg btn-black btn-block"
                onClick={this.openEditMode}
              >
                Modifica
              </button>
            }

            {
              this.state.isEditing &&
              <button
                className="btn btn-lg btn-black btn-block"
                onClick={this.saveEdits}
              >
                Annulla modifiche
              </button>
            }
          </div>
          <div className="col-md-7">
            { this.state.isEditing && this.renderUpdateForm() }
            { !this.state.isEditing && this.renderProductDetails() }
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  product: state.product,
  isLoading: state.api.loading
});

export default connect(mapStateToProps)(ProductDetailPage);
