import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import './App.css';
import logoBlack from '../img/logo-black.png';
import { search } from '../actions/search';

class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      products: []
    };
  }

  handleClickCatalogo = e => {
    e.preventDefault();
    this.props.dispatch(search({ genere: 'catalogo' }));
  }

  handleClickExtra = e => {
    e.preventDefault();
    this.props.dispatch(search({ genere: 'extra' }));
  }

  handleClickExnovo = e => {
    e.preventDefault();
    this.props.dispatch(search({ genere: 'ex_novo' }));
  }

  render() {
    return (
      <div>
        <header className="header">
          <div className="container-fluid">
            <div className="logo">
              <Link to="/">
                <img className="l-black" src={logoBlack} role="presentation" />
              </Link>
            </div>
            <nav className="navigation">
              <ul>
                <li>
                  <Link to="/new">
                    Aggiungi nuovo
                  </Link>
                </li>
                <li>
                  <a
                    href="#"
                    onClick={this.handleClickCatalogo}
                  >
                    Catalogo
                  </a>
                </li>
                <li>
                  <a
                    href="#"
                    onClick={this.handleClickExtra}
                  >
                    Extra
                  </a>
                </li>
                <li>
                  <a
                    href="#"
                    onClick={this.handleClickExnovo}
                  >
                    Ex Novo
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </header>

        <div>
          {this.props.children}
        </div>

      </div>
    );
  }
}

export default connect()(App);
