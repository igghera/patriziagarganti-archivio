import { oneLineTrim } from 'common-tags';
import { createAction } from 'redux-actions';
import { receiveData } from './api';
import config from '../config';

export const resetSearchForm = createAction('RESET_SEARCH_FORM');
export const requestSearch = createAction('SEARCH');

export function search(values) {
  return (dispatch, getState) => {
    const oldState = getState().search;
    const newState = Object.assign({}, oldState, values);

    dispatch(requestSearch(values));

    return fetch(
      oneLineTrim`${config.apiBaseUrl}/search.php?
      minWidth=${newState.larghezza.min}&
      maxWidth=${newState.larghezza.max}&
      minHeight=${newState.altezza.min}&
      maxHeight=${newState.altezza.max}&
      minDepth=${newState.profondita.min}&
      maxDepth=${newState.profondita.max}&
      minDiametro=${newState.diametro.min}&
      maxDiametro=${newState.diametro.max}&
      minLuci=${newState.luci.min}&
      maxLuci=${newState.luci.max}&
      minAnno=${newState.anno.min}&
      maxAnno=${newState.anno.max}&
      tipologia=${newState.tipologia}&
      marchio=${newState.marchio}&
      forma=${newState.forma}&
      collezione=${newState.collezione}&
      keywords=${newState.keywords}&
      accessori=${newState.accessori}&
      cliente=${newState.cliente}&
      finitura=${newState.finitura}&
      genere=${newState.genere}&
      paralume=${newState.paralume}&
      particolari=${newState.particolari}`
    )
    .then(response => response.json())
    .then(jsonResponse => dispatch(receiveData(jsonResponse)))
    .catch(error => console.log(error));
  };
}
