import { createAction } from 'redux-actions';

import {
  getAccessori,
  getClienti,
  getCollezioni,
  getFiniture,
  getForme,
  getMarchi,
  getParalumi,
  getParticolari,
  getGeneri,
  getTipologie
} from '../utils/fetchJson';

export const receivedFilters = createAction('RECEIVED_FILTERS');

export function reloadFilters() {
  return dispatch => Promise.all([
    getAccessori(),
    getClienti(),
    getCollezioni(),
    getFiniture(),
    getForme(),
    getGeneri(),
    getMarchi(),
    getParalumi(),
    getParticolari(),
    getTipologie()
  ])
  .then(values => {
    const newFilters = {
      accessori: values[0],
      clienti: values[1],
      collezioni: values[2],
      finiture: values[3],
      forme: values[4],
      generi: values[5],
      marchi: values[6],
      paralumi: values[7],
      particolari: values[8],
      tipologie: values[9]
    };

    dispatch(receivedFilters(newFilters));
    return true;
  })
  .catch();
}
