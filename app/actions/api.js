import { createAction } from 'redux-actions';
import config from '../config';

export const requestData = createAction('REQUEST_DATA');
export const receiveData = createAction('RECEIVE_DATA');
export const receiveProduct = createAction('RECEIVE_PRODUCT');

export function fetchProduct(itemId) {
  return dispatch => {
    dispatch(requestData());

    return fetch(`${config.apiBaseUrl}/productById.php?id=${itemId}`)
      .then(response => response.json())
      .then(productData => dispatch(receiveProduct(productData)));
  };
}
