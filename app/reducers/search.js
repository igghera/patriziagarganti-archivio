import { handleActions } from 'redux-actions';
import { requestSearch, resetSearchForm } from '../actions/search';
import { receiveData } from '../actions/api';

const initialState = {
  keywords: '',
  larghezza: { min: 0, max: 600 },
  altezza: { min: 0, max: 4580 },
  profondita: { min: 0, max: 277 },
  diametro: { min: 0, max: 1150 },
  luci: { min: 0, max: 156 },
  anno: { min: 2011, max: new Date().getFullYear() },
  tipologia: '',
  marchio: '',
  forma: '',
  collezione: '',
  accessori: '',
  cliente: '',
  finitura: '',
  genere: '',
  paralume: '',
  particolari: ''
};

export default handleActions({
  [requestSearch]: (state, payload) => {
    const field = Object.keys(payload.payload)[0];
    const newSearch = {
      [field]: payload.payload[field]
    };
    return Object.assign({}, state, { isSearching: true }, newSearch);
  },

  [receiveData]: (state, payload) => Object.assign({}, state, {
    isSearching: false,
    searchResults: payload.payload
  }),

  [resetSearchForm]: () => initialState
}, initialState);
