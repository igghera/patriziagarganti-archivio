import { handleActions } from 'redux-actions';
import { receiveProduct } from '../actions/api';

export default handleActions({
  [receiveProduct]: (state, { payload }) => {
    // Normalizziamo le foto
    const productPhotos = payload.photos.map(photo => {
      // Questo e' il caso in cui il prodotto e' stato inserito da Temera.
      // Il path inizia con `/`, ma non e' quello giusto perche' nella cartella
      // delle foto ci sono `/original` e `/thumbs` per cui operiamo una
      // trasformazione per puntare al file giusto
      if (photo.path.charAt(0) === '/') {
        const pathArray = photo.path.substring(1).split('/');
        const realPath = `${pathArray[0]}/original/${pathArray[1]}`;

        return { ...photo, path: realPath };
      }

      return photo;
    });

    const product = {
      ...payload,
      photos: productPhotos
    };

    return Object.assign({}, state, product);
  }
}, {});
