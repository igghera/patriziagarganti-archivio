import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import { reducer as toastrReducer } from 'react-redux-toastr';
import search from './search';
import api from './api';
import product from './product';
import filters from './filters';

const rootReducer = combineReducers({
  routing,
  search,
  api,
  product,
  filters,
  form: formReducer,
  toastr: toastrReducer
});

export default rootReducer;
