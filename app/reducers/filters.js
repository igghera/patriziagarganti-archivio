import { handleActions } from 'redux-actions';
import { receivedFilters } from '../actions/filters';

// Ogni volta che presentiamo un form (create, edit, filter) ricarichiamo i
// filtri dal DB per essere sicuri di includere eventuali nuovi filtri creati
// dall'utente

const initialState = {
  accessori: [],
  clienti: [],
  collezioni: [],
  finiture: [],
  forme: [],
  marchi: [],
  paralumi: [],
  particolari: [],
  generi: [],
  tipologie: []
};

export default handleActions({
  [receivedFilters]: (state, { payload }) => Object.assign({}, state, payload)
}, initialState);
