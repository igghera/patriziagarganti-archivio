import { handleActions } from 'redux-actions';
import {
  requestData,
  receiveData,
  receiveProduct
} from '../actions/api';

export default handleActions({
  [requestData]: state => Object.assign({}, state, { loading: true }),
  [receiveData]: state => Object.assign({}, state, { loading: false }),
  [receiveProduct]: state => Object.assign({}, state, { loading: false })
}, {});
